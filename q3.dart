void main() {
  var app = App();
  app.appName = "FNB Banking";
  app.year = 2012;
  app.category = "Finace";
  app.developer = "Jordan Gallant";
  print(app.makeUpper(app.appName));
  print(app.category);
  print(app.developer);
  print(app.year);
}

class App {
  String appName = "";
  String category = "";
  String developer = "";
  int year = 0;

  makeUpper(String s) {
    return s.toUpperCase();
  }
}
